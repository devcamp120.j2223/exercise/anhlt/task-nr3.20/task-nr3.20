//B1: khai báo sử dụng mongoose
const mongoose = require("mongoose");

//B2: khai báo schema
const Schema = mongoose.Schema;

//B3: khơi tạo 1 schema
const userSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    name : {
        type: String,
        required: true
    },
    username : {
        type: String,
        unique:true
    },
    email : String,
    address : {
        street : {
            type: String,
            required: true
        },
        suite : {
            type: String,
            required: true
        },
        city : {
            type: String,
            required: true
        },
        zipcode : {
            type: String,
            required: true
        },
        geo : {
            lat : {
                type: String,
                required: true
            },
            lng : {
                type: String,
                required: true
            }
        }
    },
    phone: String,
    website : String,
    company : {
        name : {
            type: String,
            required: true
        },
        catchPhrase : String,
        bs : String
    }
})

//B4: export model compile từ schema
module.exports = mongoose.model("user", userSchema);
